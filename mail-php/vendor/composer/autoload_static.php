<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1536d0ac05209beae84bd117a1083eb1
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Socketlabs\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Socketlabs\\' => 
        array (
            0 => __DIR__ . '/..' . '/socketlabs/email-delivery/InjectionApi/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1536d0ac05209beae84bd117a1083eb1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1536d0ac05209beae84bd117a1083eb1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
