(function ($) {

	"use strict";

	//Hide Loading Box (Preloader)
	function handlePreloader() {
		if ($('.preloader').length) {
			$('.preloader').delay(200).fadeOut(500);
		}
	}

	$("#checkexisting").click(function() {
			if ($(this).is(":checked")) {
					$("#existingList").show();
			} else {
					$("#existingList").hide();

			}
	});

	$("#checkSteps").click(function() {
			if ($(this).is(":checked")) {
					$("#stairList").show();
			} else {
					$("#stairList").hide();
			}
	});


	//Update Header Style and Scroll to Top
	function headerStyle() {
		if ($('.main-header').length) {
			var windowpos = $(window).scrollTop();
			var siteHeader = $('.main-header');
			var scrollLink = $('.scroll-to-top');
			if (windowpos >= 250) {
				siteHeader.addClass('fixed-header');
				scrollLink.fadeIn(300);
			} else {
				siteHeader.removeClass('fixed-header');
				scrollLink.fadeOut(300);
			}
		}
		var windowpos = $(window).scrollTop();
		var scrollLink = $('.scroll-to-bot');
		if (scrollLink[0]) {
			if (windowpos >= 249) {
				scrollLink.fadeOut(300);
			} else {
				scrollLink.fadeIn(300);
				scrollLink[0].style.display = 'inline-block';
			}
		}

	}

	headerStyle();


	//Submenu Dropdown Toggle
	if ($('.main-header .navigation li.dropdown ul').length) {
		$('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-down"></span></div>');

		//Dropdown Button
		$('.main-header .navigation li.dropdown .dropdown-btn').on('click', function () {
			$(this).prev('ul').slideToggle(500);
		});

		//Disable dropdown parent link
		$('.navigation li.dropdown > a').on('click', function (e) {
			e.preventDefault();
		});
	}


	//Revolution Slider
	function revolve() {

		if ($('.main-slider .tp-banner').length) {

			var MainSlider = $('.main-slider');
			var strtHeight = MainSlider.attr('data-start-height');
			var slideOverlay = "'" + MainSlider.attr('data-slide-overlay') + "'";

			$('.main-slider .tp-banner').show().revolution({
				dottedOverlay: slideOverlay,
				delay: 20000,
				startwidth: 1200,
				startheight: strtHeight,
				hideThumbs: 600,

				thumbWidth: 80,
				thumbHeight: 50,
				thumbAmount: 5,

				navigationType: "bullet",
				navigationArrows: "0",
				navigationStyle: "preview3",

				touchenabled: "on",
				onHoverStop: "off",

				swipe_velocity: 0.7,
				swipe_min_touches: 1,
				swipe_max_touches: 1,
				drag_block_vertical: false,

				parallax: "mouse",
				parallaxBgFreeze: "on",
				parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

				keyboardNavigation: "off",

				navigationHAlign: "center",
				navigationVAlign: "bottom",
				navigationHOffset: 0,
				navigationVOffset: 50,

				soloArrowLeftHalign: "left",
				soloArrowLeftValign: "bottom",
				soloArrowLeftHOffset: 20,
				soloArrowLeftVOffset: 0,

				soloArrowRightHalign: "right",
				soloArrowRightValign: "center",
				soloArrowRightHOffset: 20,
				soloArrowRightVOffset: 0,

				shadow: 0,
				fullWidth: "on",
				fullScreen: "off",

				spinner: "spinner4",

				stopLoop: "off",
				stopAfterLoops: -1,
				stopAtSlide: -1,

				shuffle: "off",

				autoHeight: "off",
				forceFullWidth: "on",

				hideThumbsOnMobile: "on",
				hideNavDelayOnMobile: 1500,
				hideBulletsOnMobile: "on",
				hideArrowsOnMobile: "on",
				hideThumbsUnderResolution: 0,

				hideSliderAtLimit: 0,
				hideCaptionAtLimit: 0,
				hideAllCaptionAtLilmit: 0,
				startWithSlide: 0,
				videoJsPath: "",
				fullScreenOffsetContainer: ""
			});
		}

	}
	revolve();

	//Price Range Slider
	if ($('.range-slider-price').length) {

		var priceRange = document.getElementById('range-slider-price');

		noUiSlider.create(priceRange, {
			start: [180, 560],
			limit: 1000,
			behaviour: 'drag',
			connect: true,
			range: {
				'min': 100,
				'max': 1000
			}
		});

		var limitFieldMin = document.getElementById('min-value-rangeslider');
		var limitFieldMax = document.getElementById('max-value-rangeslider');

		priceRange.noUiSlider.on('update', function (values, handle) {
			(handle ? limitFieldMax : limitFieldMin).value = values[handle];
		});
	}


	// Product Carousel Slider
	if ($('.shop-page .image-carousel').length && $('.shop-page .thumbs-carousel').length) {

		var $sync1 = $(".shop-page .image-carousel"),
			$sync2 = $(".shop-page .thumbs-carousel"),
			flag = false,
			duration = 500;

		$sync1
			.owlCarousel({
				loop: false,
				items: 1,
				margin: 0,
				nav: false,
				navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
				dots: false,
				autoplay: true,
				autoplayTimeout: 5000
			})
			.on('changed.owl.carousel', function (e) {
				if (!flag) {
					flag = false;
					$sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
					flag = false;
				}
			});

		$sync2
			.owlCarousel({
				loop: false,
				margin: 10,
				items: 1,
				nav: true,
				navText: ['<span class="icon flaticon-arrows-8"></span>', '<span class="icon flaticon-arrows-9"></span>'],
				dots: false,
				center: false,
				autoplay: true,
				autoplayTimeout: 5000,
				responsive: {
					0: {
						items: 3,
						autoWidth: false
					},
					400: {
						items: 3,
						autoWidth: false
					},
					600: {
						items: 3,
						autoWidth: false
					},
					900: {
						items: 5,
						autoWidth: false
					},
					1000: {
						items: 5,
						autoWidth: false
					}
				},
			})

			.on('click', '.owl-item', function () {
				$sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
			})
			.on('changed.owl.carousel', function (e) {
				if (!flag) {
					flag = true;
					$sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
					flag = false;
				}
			});
	}


	//Jquery Spinner / Quantity Spinner
	if ($('.quantity-spinner').length) {
		$("input.quantity-spinner").TouchSpin({
			verticalbuttons: true
		});
	}

	//Tabs Box
	if ($('.tabs-box').length) {
		$('.tabs-box .tab-buttons .tab-btn').on('click', function (e) {
			e.preventDefault();
			var target = $($(this).attr('data-tab'));

			if ($(target).is(':visible')) {
				return false;
			} else {
				target.parents('.tabs-box').find('.tab-buttons').find('.tab-btn').removeClass('active-btn');
				$(this).addClass('active-btn');
				target.parents('.tabs-box').find('.tabs-content').find('.tab').fadeOut(0);
				target.parents('.tabs-box').find('.tabs-content').find('.tab').removeClass('active-tab');
				$(target).fadeIn(300);
				$(target).addClass('active-tab');
			}
		});
	}


	// Sponsors Carousel
	if ($('.sponsors-carousel').length) {
		$('.sponsors-carousel').owlCarousel({
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 500,
			autoplay: 4000,
			navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				600: {
					items: 3
				},
				800: {
					items: 3
				},
				1024: {
					items: 4
				}
			}
		});
	}


	//Single Item Carousel
	if ($('.single-item-carousel').length) {
		$('.single-item-carousel').owlCarousel({
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 700,
			autoplay: 5000,
			navText: ['<span class="flaticon-left-arrow-2"></span>', '<span class="flaticon-right-arrow-1"></span>'],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				800: {
					items: 1
				},
				1024: {
					items: 1
				},
				1200: {
					items: 1
				}
			}
		});
	}


	//Three Item Carousel
	if ($('.three-item-carousel').length) {
		$('.three-item-carousel').owlCarousel({
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 700,
			autoplay: 5000,
			navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				600: {
					items: 2
				},
				1024: {
					items: 3
				},
				1200: {
					items: 3
				}
			}
		});
	}


	//Four Item Carousel
	if ($('.four-item-carousel').length) {
		$('.four-item-carousel').owlCarousel({
			loop: true,
			margin: 0,
			nav: true,
			smartSpeed: 700,
			autoplay: 5000,
			navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				800: {
					items: 2
				},
				1024: {
					items: 3
				},
				1200: {
					items: 4
				}
			}
		});
	}


	//Team Carousel
	if ($('.team-carousel').length) {
		$('.team-carousel').owlCarousel({
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 700,
			autoplay: 5000,
			navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 2
				},
				1024: {
					items: 3
				},
				1200: {
					items: 4
				}
			}
		});
	}


	//Projects Carousel
	if ($('.projects-carousel').length) {
		$('.projects-carousel').owlCarousel({
			loop: true,
			margin: 30,
			nav: true,
			smartSpeed: 700,
			autoplay: 5000,
			navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 2
				},
				1024: {
					items: 3
				},
				1200: {
					items: 4
				}
			}
		});
	}


	//Gallery Filters
	if ($('.filter-list').length) {
		$('.filter-list').mixItUp({});
	}


	//LightBox / Fancybox
	if ($('.lightbox-image').length) {
		$('.lightbox-image').fancybox({
			openEffect: 'fade',
			closeEffect: 'fade',
			helpers: {
				media: {}
			}
		});
	}

	//Contact Form Validation
	if ($('#contact-form').length) {
		$('#contact-form').validate({
			rules: {
				firstname: {
					required: true
				},
				lastname: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				message: {
					required: true
				}
			}
		});
	}

	// Scroll to a Specific Div
	if ($('.scroll-to-target').length) {
		$(".scroll-to-target").on('click', function () {
			var target = $(this).attr('data-target');
			// animate
			$('html, body').animate({
				scrollTop: $(target).offset().top
			}, 1000);

		});
	}


	// Elements Animation
	if ($('.wow').length) {
		var wow = new WOW({
			boxClass: 'wow', // animated element css class (default is wow)
			animateClass: 'animated', // animation css class (default is animated)
			offset: 0, // distance to the element when triggering the animation (default is 0)
			mobile: false, // trigger animations on mobile devices (default is true)
			live: true // act on asynchronously loaded content (default is true)
		});
		wow.init();
	}

	/* ==========================================================================
	   When document is Scrollig, do
	   ========================================================================== */

	$(window).on('scroll', function () {
		headerStyle();
	});

	/* ==========================================================================
	   When document is loaded, do
	   ========================================================================== */

	$(window).on('load', function () {
		handlePreloader();
	});

})(window.jQuery);











function calc() {

		var opt = $("#calc-material").val();
		var removal = $("#existing").val();

		var unit = 59;

		var cost = $('#floor-area').val() * unit;

		if($("#checkexisting").is(":checked")){
			var unitRemove = 0;

			switch (removal) {
				case 'carpet':unitRemove = 6;
					break;
					case 'timber':unitRemove = 7;
						break;
						case 'vinyl':unitRemove = 7;
							break;
							case 'tile':unitRemove = 7.5;
								break;
				default: unitRemove = 0;
			}

			cost += $('#floor-area').val() * unitRemove;
		}

		if($("#checkSteps").is(":checked")){
			cost += $("#stepz").val() * 55;
		}

		if(cost <= 550){ cost = 550; }

		$('#est-price').html(cost);



		//$("#existingList").hide();
		//$("#stairList").hide();
		//$('#getQuet').trigger("reset");
		$("#quetPrice").show();
		$("#getQuet").css({
				opacity: 0.1
		});


}

function closePopup() {
		$("#existingList").hide();
		//$('#getQuet').trigger("reset");
		$("#quetPrice").hide();
		$("#getQuet").css({
				opacity: 1
		});
}

function k() {
		if(undefined != $("#q-mail-in").val() && null != $("#q-mail-in").val() && "" != $("#q-mail-in").val()){
			$('#getQuet').submit();
			closePopup();
			alert('A quotation will be sent to you via email!<br>It may take upto 1 working day.');
		}else{
			alert('Please enter a valid email address!');
		}


}

function goTo(x) {
		if (x == "shop") {
				window.location.replace("shop.html");
		} else {
				window.location.replace("services.html");
		}
}

$( "#calc-material" ).change(function() {
	var opt = $("#calc-material").val();

	var ajaxURL = 'https://array-db.firebaseio.com/';

	switch (opt) {
		case 'lami8':
				ajaxURL += '8mm';
			break;
		case 'lami10':
				ajaxURL += '10mm';
				break;
			case 'lami12':
					ajaxURL += '12mm';
					break;
			case 'lami12w':
						ajaxURL += '12mmw';
						break;
			case 'lamiVin':
							ajaxURL += 'vinyl';
							break;
		default:
					ajaxURL += '8mm';
	}

	$.get(ajaxURL + ".json", function(data, status){

		if(null == data){
			//alert('item not found!');
		}

		$("#calc-colour option").remove();

		$.each(data, function (i, item) {
			if('@' === item || '' === item){ return false;
}else{
    $('#calc-colour').append($('<option>', {
        value: item,
        text : item
    }));
	}
});

	});



});



$("#q-mail-in").change(function(){
  $("#q-mail").val($("#q-mail-in").val())
});
